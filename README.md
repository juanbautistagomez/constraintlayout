# ConstraintLayout #

ConstraintLayout permite crear UI responsivos, permite crear grandes y complejos diseños con una jerarquía de vistas plana tiene una similitud al RelativeLayout aunque es mas facil de usa en el entorno graficoo diseño de android studio
el constrain layout esta disponible en las herrramientas visuales del editor  ya que estos estan dieñados para funcionar conjuntamente asi se puede crear el diseño arrastrando y soltando los elementos en lugar de editar el XML

### Biblioteca de constraintLayout

implementation "androidx.constraintlayout:constraintlayout:2.0.0-rc1"

Se agrega en el gradle como dependencia en este caso la version mas reciente es funcional con androidx 
para mas información podes consultar la documentación en 

[Documentation android ConstraintLayout](https://developer.android.com/training/constraint-layout)
